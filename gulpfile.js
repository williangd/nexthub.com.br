const gulp = require('gulp');
const gzip = require('gulp-gzip');
const imagemin = require('gulp-imagemin');
const pngquant = require('gulp-pngquant');

gulp.task('compress-css', () => {
    return gulp.src(['./public/assets/css/main.css', './public/assets/css/main.css.map'])
        .pipe(gzip())
        .pipe(gulp.dest('./public/assets/css/'));
});

gulp.task('compress-img', ['pngquant'], () => {
    return gulp.src('./public/assets/img/*.png')
        .pipe(gzip())
        .pipe(gulp.dest('./public/assets/img/'));
});

gulp.task('compress-js', () => {
    return gulp.src('./public/assets/js/*.js')
        .pipe(gzip())
        .pipe(gulp.dest('./public/assets/js/'));
});

gulp.task('compress-html', () => {
    return gulp.src('./public/index.html')
        .pipe(gzip())
        .pipe(gulp.dest('./public/'));
});

gulp.task('pngquant', () => {
    return gulp.src('assets/img/*.png')
        .pipe(pngquant({
            quality: '95-100'
        }))
        .pipe(gulp.dest('public/assets/img/'))
});

gulp.task('default', ['pngquant', 'compress-css', 'compress-img', 'compress-js', 'compress-html']);